// SPDX-License-Identifier: BSD-3-Clause
/*
 * This file is part of CC33XX
 *
 * Copyright(c) 2012 Texas Instruments Incorporated - https://www.ti.com/
 */

uint32_t calc_crc32(char *buffer, size_t len);
